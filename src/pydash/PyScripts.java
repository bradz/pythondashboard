package pydash;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.InputStreamReader;
import java.util.Vector;

public class PyScripts {
	
	final String NONE = "none";
	String logFile = NONE;
	String pyFile = NONE;
	String ppid = NONE;
	String pid = NONE;
	String user = NONE;
	String name = NONE;
	String cpu = NONE;
	String mem = NONE;
	String rss = NONE;
	String vsz = NONE;
	String stat = NONE;
	String started = NONE;

	
	// TODO: 
	// lookup proc
	// cat /proc/pid/cmdline
	/**/
	
	public static Vector<PyScripts> getRunningPythonScripts() {
		Vector<PyScripts> scripts = new Vector<PyScripts>();
		try {	
			String[] cmd = new String[]{"/bin/sh", "-c", "ps -fC python"};
			Process proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
			while ((line = procReader.readLine()) != null) {
				if(line.trim().length() > 0) {
					if( ! line.startsWith("UID")) scripts.add( new PyScripts(line));
				}
			}
			
			cmd = new String[]{"/bin/sh", "-c", "ps -aux | grep python"};
			proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
			line = null;
			procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
			while ((line = procReader.readLine()) != null) {
				if(line.trim().length() > 0) {
					if ( !line.contains("grep"))
						addDetails(scripts, line);
				}
			}
		} catch (Exception e) { Util.printError(e); }		
		return scripts;
	}	
	
	public static boolean isPythonPid(final String pid){
		if ( ! Util.isInteger(pid)) return false;
		
		String line = null;
		try {	
			
			String[] cmd = new String[]{"/bin/sh", "-c", "pidof python"};
			Process proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
			
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
			line = procReader.readLine();
			
		} catch (Exception e) { Util.printError(e); return false; }
		
		final String tokens[] = line.trim().split("\\s+");
		for( int i = 0; i < tokens.length ; i++)
			if (tokens[i].equals(pid)) return true;
		
		return false;
	}

	
	/* Add details like cpu and memory, match pids 
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
brad      7583 81.5  0.7  10612  7032 pts/3    R+   16:44  68:52 python battery_monitor.py
brad     18032  0.0  0.7  10620  7104 pts/1    S+   18:05   0:00 python docked_detect.py
	 */
	static Vector<PyScripts> addDetails(Vector <PyScripts> pys, final String line){
		final String tokens[] = line.trim().split("\\s+");
		for (int i = 0 ; i < pys.size() ; i++){
			
//			for( int j = 1 ; j < tokens.length; j++ ) 
//				System.out.println("tokens[" + j + "]: " + tokens[j]);
			
//			System.out.println("looking for pid: " + s.pid);
		
			PyScripts s = pys.get(i);
			if (s.pid.equals(tokens[1])){
			
				/*
			System.out.println("\r\n" + line + " " + tokens.length);
			System.out.println("tokens[1]: " + tokens[1]);
			System.out.println("tokens[2]: " + tokens[2]);
			System.out.println("tokens[3]: " + tokens[3]);
			System.out.println("tokens[4]: " + tokens[4]);
			System.out.println("tokens[11]: " + tokens[11]);
			*/
		//	System.out.println(s.pid + " == " + tokens[1] + " cpu: " + tokens[2] + " rss: " + tokens[5] + " mem: " + tokens[3]);

				s.cpu = tokens[2];
				s.mem = tokens[3];
				s.vsz = tokens[4];
				s.rss = tokens[5];
				s.stat = tokens[7];
				s.started = tokens[8];
				
				
			}
		}
		return pys;
	}
	
	/*
	static File[] getScriptFiles(){
		File telnet = new File(Settings.telnetscripts);
		if( ! telnet.exists()) telnet.mkdir();
		File[] names = telnet.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) { 
			
				if(pathname.getName().endsWith("oculusprimesocket.py")) return false;
				
				if(pathname.getName().startsWith("startup_")) return false;
				
				return pathname.getName().endsWith(".py"); 
				
			}
		});
		return names;
	}
	*/


	// TODO: 
	// lookup proc
	// cat /proc/pid/cmdline
	
	PyScripts(String line){
		
		final String tokens[] = line.trim().split("\\s+");
		
		// sanity 
		if(tokens.length < 8) { 
			Util.log("PID type? " + tokens[7], this);  
			return; 
		}
		
/*		
		if( ! tokens[7].contains("python")) {
			Util.log("ERROR -- PID type? " + tokens[7], this); 
			return;
		}*/
		if( ! Util.isInteger(tokens[1])){
			Util.log("ERROR -- PID is not valid? " + tokens[7], this); 
			return;
		}
		
		if( ! Util.isInteger(tokens[2])){
			Util.log("ERROR -- PPID is not valid? " + tokens[7], this); 
			return;
		}
	
		
//		Util.log(tokens.length + " tokens:" + line);
//		for(int i = 6 ; i < tokens.length ; i++ ) Util.log(i+" == " +tokens[i]);
//		Util.log(tokens.length + " = " +tokens[tokens.length-1]);
		
		user = tokens[0];
		pid = tokens[1];
		ppid = tokens[2];
				
		if(tokens.length >= 9) {
			pyFile = tokens[8];
			name = tokens[8];
			if(name.contains("/")) name = name.substring(name.lastIndexOf("/")+1);//, name.indexOf(".py"));
		}
		
		if(tokens.length >= 11) {	
			logFile = tokens[10].replaceAll("__log:=", "");
//			Util.log(logFile, this);
		}
		
//		if(logFile.contains("/")) logFile = logFile.substring(logFile.lastIndexOf("/")+1); // , logFile.lastIndexOf(".log"));
		
//		if(pyFile.equals(NONE)) pyFile = "error";
//		if(logFile.equals(NONE)) logFile = "none";
	
//		Util.log("log: "+logFile, this);
	}


/*

486537347049, static, 0 = brad 
1486537347050, static, 1 = 4197 
1486537347050, static, 2 = 4170 
1486537347050, static, 3 = 7 
1486537347050, static, 4 = 23:01 
1486537347050, static, 5 = ? 
1486537347050, static, 6 = 00:00:05 
1486537347050, static, 7 = python 
1486537347050, static, 8 = /home/brad/catkin_ws/src/oculusprime_ros/src/arcmove_globalpath_follower.py 
1486537347050, static, 9 = __name:=arcmove_globalpath_follower 
1486537347050, static, 10 = __log:=/home/brad/.ros/log/5c59de64-edcc-11e6-8465-b803054ce181/arcmove_globalpath_follower-2.log 
1486537347051, static, str tokens = 11 
1486537347051, static, 0 = brad 
1486537347051, static, 1 = 4466 
1486537347051, static, 2 = 4170 
1486537347051, static, 3 = 8 
1486537347051, static, 4 = 23:01 
1486537347051, static, 5 = ? 
1486537347051, static, 6 = 00:00:05 
1486537347052, static, 7 = python 
1486537347052, static, 8 = /home/brad/catkin_ws/src/oculusprime_ros/src/remote_nav.py 
1486537347052, static, 9 = __name:=remote_nav 
1486537347052, static, 10 = __log:=/home/brad/.ros/log/5c59de64-edcc-11e6-8465-b803054ce181/remote_nav-17.log 

	
1486602981623, static, 0 = brad 
1486602981624, static, 1 = 5749 
1486602981624, static, 2 = 1261 
1486602981625, static, 3 = 27 
1486602981625, static, 4 = 16:23 
1486602981625, static, 5 = ? 
1486602981625, static, 6 = 00:14:19 
1486602981626, static, 7 = python
1486602981626, static, 8 = telnet_scripts/block.py 

*/

	
	
}