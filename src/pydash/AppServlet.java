package pydash;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AppServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	static final int MAX_ROWS = 6;
	static final int MAX_COLS = 9;
	private static String[][] data = new String[MAX_ROWS][MAX_COLS];
    private static String content = null;

    public AppServlet() {
    	Util.log("..........started.........", this);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException { doPost(req,res); }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = null;
        try { action = request.getParameter("action");
        } catch (Exception e){}
        
        String pid = null;
        try { pid = request.getParameter("pid");
        } catch (Exception e){}
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if (action == null) {
        	if (content == null) content = getHTML();
            out.println(content);
            return;
        }
        
        if (!action.equals("getinfo")) Util.log(action);

        switch (action) {
               
        	case "kill": if (PyScripts.isPythonPid(pid)) Util.killPID(pid);	break;
            case "getinfo": out.println(getInfo()); break;
            case "shutdownhnow": shutdownhnow(); break;
            case "reboot": reboot(); break;
            
            // reload 
            case "test": content = getHTML(); break;
        }
    }

    private String getHead() {
    	String text = "\r\n<head>\r\n";  	
    	try {
			text += getScript();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	text += getCSS();
    	text += "\r\n</head>\r\n";    	
    	return text;
    }

    private String getScript() throws Exception {
    	
    	String text = "\r\n<script type=\"text/javascript\"> \r\n";

    	File js = new File("webapps/pydash/script.js");
    	if (js.exists()) {
    		
    		System.out.println("...... using: " + js.getAbsolutePath());
    		String line;
            FileInputStream filein = new FileInputStream(js);
            BufferedReader reader = new BufferedReader(new InputStreamReader(filein));
            while ((line = reader.readLine()) != null) text += line+"\n";
            reader.close();
            filein.close();

    	} else {
    		System.out.println("no js, fatal.. die");
    		System.exit(-1);
    	}
    	
    	/*else {
			
    		System.out.println("use defaults ....not found: " + js.getAbsolutePath()); 
			text += "\r\n var GETINFOINTERVAL = 900;";
			text += "\r\n function loaded() {getInfo();}";
			text += "\r\n function openxmlhttp(theurl, functionname) {";
			text += "\r\n if (window.XMLHttpRequest) {";
			text += "\r\n xmlhttp=new XMLHttpRequest();} ";
			text += "\r\n else if (window.ActiveXObject) {";
			text += "\r\n xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");";
			text += "\r\n theurl += \"?\" + new Date().getTime(); } ";
			text += "\r\n if (xmlhttp!=null) {";
			text += "\r\n xmlhttp.onreadystatechange=functionname; // event handler function call;";
			text += "\r\n xmlhttp.open(\"GET\",theurl,true);";
			text += "\r\n xmlhttp.send(null); ";
			text += "\r\n } else { alert(\"Your browser does not support XMLHTTP.\"); }}";
			text += "\r\n ";
			text += "\r\n function getInfo() { openxmlhttp(\"?action=getinfo\", infoReceived); setTimeout(\"getInfo();\", GETINFOINTERVAL); }";
			text += "\r\n ";
			text += "\r\n function infoReceived() {";
			text += "\r\n if (xmlhttp.readyState==4) { if (xmlhttp.status==200) { eval(xmlhttp.responseText); }}} ";
			text += "\r\n ";
			text += "\r\n function inrlink(id, str) { if (document.getElementById(id) != null) document.getElementById(id).innerHTML = \"<a href=''>killz</a> \" + str; }";	
			text += "\r\n function inrhtml(id, str) { if (document.getElementById(id) != null) document.getElementById(id).innerHTML = str; }";
			text += "\r\n ";
    	}*/
    	
   
    	text += "\r\n</script>\r\n";
		return text;
	}

    private String getCSS() {
   
    	String text = "\r\n <style type=\"text/css\" > table {text-align: left; border: 1px solid grey; min-width: 800px; max-width: 600px; padding: 5px; } </style> \r\n ";
    	return text;
    }

    private String getBody() {

    	String text = "\r\n";
    	text += "\r\n <body onload=\"loaded();\">";
    	text += "\r\n <span id=\"current\"></span>";
    	text += getTable();
    	
    //	text +=    "<button type=\"button\" onclick=\"cmdsend('die')\">     die    </button> "; 
    	text +=    "<button type=\"button\" onclick=\"cmdsend('shutdownhnow')\"> power off </button> "; 
    	text +=    "<button type=\"button\" onclick=\"cmdsend('reboot')\">  reboot  </button> "; 
    //	text += "\r\n <br><button type=\"button\" onclick=\"cmdsend('test')\"> reload </button> \r\n";

		return text += "\r\n</body>\r\n";
    }


    private String getTable() {

    	String text = "\r\n ";
    	text += "\r\n <table>";
    	text += "\r\n <tr><th>process</th><th>script</th><th>user</th><th>mem</th><th>cpu</th><th>vsz</th><th>rss</th><th>stat</th><th>start</th></tr>";

    	for( int i = 0 ; i < MAX_ROWS ; i++ ){
    		String line = "\r\n\t<tr>"; 
    		for( int j = 0 ; j < MAX_COLS ; j++){
    			data[i][j] = "";
    			line += "<td><span id=\"c" + i + "-"+ j + "\">" +data[i][j]+ "</span></td>"; 
    		}
    		
    		line += "</tr> \r\n";
    		text += line;
    	
    	}
    
		return text += "\r\n</table></p>\r\n";
    }
    	
    
    
    private String getHTML() {
    	String text = "<html>\r\n";  	
    	text += getHead();
    	text += getBody();
		return text += "\r\n</html>\r\n";
	}

	private String getInfo() {
			
        String response = "inrhtml('current','"+ Util.getUptime() + ", cpu: " + Util.getCPU() +"');";
		Vector<PyScripts> scripts = PyScripts.getRunningPythonScripts();
       
		if (scripts != null){
			
			// clear table 
			if (scripts.size() == 0){ 
				response += "inrhtml('pids','0');";
				for( int i = 0 ; i < MAX_ROWS ; i++ ){
		    		for( int j = 0 ; j < MAX_COLS ; j++){
		    			response += "inrhtml('c"+ i + "-" + j +"','');";
		    		}
				}
			}
			
			if (scripts.size()  > 0){
		    	for( int i = 0 ; i < scripts.size() ; i++ ){
		    		
		    		response += "inrlink('c"+ i+"-0','"+ scripts.get(i).pid   +"');";
		    		response += "inrhtml('c"+ i+"-1','"+ Util.trimLength(scripts.get(i).name, 25)  +"');";
		    		response += "inrhtml('c"+ i+"-2','"+ scripts.get(i).user  +"');";
		    		response += "inrhtml('c"+ i+"-3','"+ scripts.get(i).mem   +"');";
		    		response += "inrhtml('c"+ i+"-4','"+ scripts.get(i).cpu   +"');";
		    		response += "inrhtml('c"+ i+"-5','"+ scripts.get(i).vsz   +"');";
		    		response += "inrhtml('c"+ i+"-6','"+ scripts.get(i).rss   +"');";
		    		response += "inrhtml('c"+ i+"-7','"+ scripts.get(i).stat   +"');";
		    		response += "inrhtml('c"+ i+"-8','"+ scripts.get(i).started   +"');";

		    	}
		
		    	// delete old
		    	for( int i = scripts.size() ; i < MAX_ROWS ; i++ ){
		    		for( int j = 0 ; j < MAX_COLS ; j++){
		    			response += "inrhtml('c"+ i + "-" + j +"','');";
		    		}
		    	}
			}
		}

        return response;
    }

	
    private void reboot() {
        try { Runtime.getRuntime().exec("sudo reboot now");
        } catch (Exception e) { e.printStackTrace(); }
    }
    private void shutdownhnow() {
        try { Runtime.getRuntime().exec("/usr/bin/sudo /sbin/shutdown -h now");
        } catch (Exception e) { e.printStackTrace(); }
    }
}
