package pydash;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

public class Util {

	public static int c = 0;
	public static final int PRECISION = 1;	
	
	public static String trimLength(String txt, int length){
		if(txt.length() > length) {
			txt = txt.substring(0, length);
			txt += "..";
		}
		return txt.trim();
	}
	
	public static void delay(long delay) {
		try { Thread.sleep(delay); } 
		catch (Exception e){ printError(e); }
	}

	public static void delay(int delay) {
		try { Thread.sleep(delay); } 
		catch (Exception e){ printError(e); }
	}

	public static String getTime() {
        Date date = new Date();
		return date.toString();
	}

	public static String getDateStamp() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}
	
	public static String getDateStampShort() {
		DateFormat dateFormat = new SimpleDateFormat("mm-ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}
	
	/**
	 * Returns the specified double, formatted as a string, to n decimal places,
	 * as specified by precision.
	 * <p/>
	 * ie: formatFloat(1.1666, 1) -> 1.2 ie: formatFloat(3.1666, 2) -> 3.17 ie:
	 * formatFloat(3.1666, 3) -> 3.167
	 */
	public static String formatFloat(double number, int precision) {

		String text = Double.toString(number);
		if (precision >= text.length()) {
			return text;
		}

		int start = text.indexOf(".") + 1;
		if (start == 0) return text;
		if (precision == 0) return text.substring(0, start - 1);
		if (start <= 0) return text;
		else if ((start + precision) <= text.length()) 
			return text.substring(0, (start + precision));
		else return text;
	}

	public static String formatFloat(String text, int precision) {
		int start = text.indexOf(".") + 1;
		if (start == 0) return text;
		if (precision == 0) return text.substring(0, start - 1);
		if (start <= 0) {
			return text;
		} else if ((start + precision) <= text.length()) {
			return text.substring(0, (start + precision));
		} else return text;
	}
	
	/**
	 * Returns the specified double, formatted as a string, to n decimal places,
	 * as specified by precision.
	 * <p/>
	 * ie: formatFloat(1.1666, 1) -> 1.2 ie: formatFloat(3.1666, 2) -> 3.17 ie:
	 * formatFloat(3.1666, 3) -> 3.167
	 */
	public static String formatFloat(double number) {

		String text = Double.toString(number);
		if (PRECISION >= text.length()) { return text; }
		int start = text.indexOf(".") + 1;
		if (start == 0)	return text;
		if (start <= 0) { return text; }
		else if ((start + PRECISION) <= text.length()) { return text.substring(0, (start + PRECISION)); } 
		else return text;
	}

	/**
	 * Run the given text string as a command on the host computer. 
	 * 
	 * @param args is the command to run, like: "restart
	 * 
	 */
	public static void systemCallBlocking(final String args) {
		try {	

			log("systemCallBlocking(): " + args);
			Process proc = Runtime.getRuntime().exec(args);

//			long start = System.currentTimeMillis();
//			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
//			String line = null;
//			System.out.println(proc.hashCode() + "OCULUS: exec():  " + args);
//			while ((line = procReader.readLine()) != null)
//				System.out.println(proc.hashCode() + " systemCallBlocking() : " + line);
			
			proc.waitFor(); // required for linux else throws process hasn't terminated error

//			System.out.println("OCULUS: process exit value = " + proc.exitValue());
//			System.out.println("OCULUS: blocking run time = " + (System.currentTimeMillis()-start) + " ms");

		} catch (Exception e) {
			printError(e);
		}
	}	
	
	public static boolean isInteger(String s) {
	    try { Integer.parseInt(s); } catch(Exception e) { 
	        return false; 
	    }
	    return true;
	}
	
	public static boolean isDouble(String s) {
	    try { Double.parseDouble(s); } catch(Exception e) { 
	        return false; 
	    }
	    return true;
	}
	
	public static Vector<String> tail(String path, int lines) {
		Vector<String> tail = new Vector<String>();
		try {	
			String[] cmd = new String[]{"/bin/sh", "-c", "tail -n " + lines + " " + path};
			Process proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
			while ((line = procReader.readLine()) != null) {
				if(line.trim().length() > 0) tail.add(line);
			}
		} catch (Exception e) { log(e.getMessage()); printError(e); }
		return tail;
	}	
	/*
	public static Vector<String> tail(String path, String match, int lines) {
		Vector<String> tail = new Vector<String>();
		try {	
			String[] cmd = new String[]{"/bin/sh", "-c", "grep " + match + " " + path + " | tail -n " + lines};
			Process proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
			while ((line = procReader.readLine()) != null) {
				if(line.trim().length() > 0) tail.add(line);
			}
		} catch (Exception e) { log(e.getMessage()); printError(e); }
		return tail;
	}	
	*/
	/**
	 * Run the given text string as a command on the windows host computer. 
	 * 
	 * @param str is the command to run, like: "restart"
	 */
	public static void systemCall(final String str){
		log("systemCall: " + str);
		if(str == null) return;
		try { Runtime.getRuntime().exec(str); 
		} catch (Exception e) { printError(e); }
	}

	public static String readUrlToString(String urlString) {
		try {
			URL website = new URL(urlString);
			URLConnection connection = website.openConnection();
			BufferedReader in = new BufferedReader( new InputStreamReader( connection.getInputStream()));

			StringBuilder response = new StringBuilder();
			String inputLine;

			while ((inputLine = in.readLine()) != null) response.append(inputLine);
			in.close();
			return response.toString();

		} catch (Exception e) {
			// Util.debug("Util.readUrlToString() parse error"); 
			return null;
		}
	}
	
	
	public static void log(String str){
		log(str, null);
	}
	
	public static void log(String method, Exception e, Object c) {
		log(method + ": " + e.getLocalizedMessage(), c);
	}
	
	public static void log(String str, Object c) {
    	if(str == null) return;
		String filter = "static";
		if(c!=null) filter = c.getClass().getName();
		System.out.println(getTime() + ", " + filter + ", " + str);
	}
	/*
    public static void debug(String str, Object c) {
    	if(str == null) return;
    	String filter = "static";
    	if(c!=null) filter = c.getClass().getName();
		if(Settings.getReference().getBoolean("debug")) {
			System.out.println("DEBUG: " + getTime() + ", " + filter +  ", " +str);
		}
	}
    
    public static void debug(String str) {
    	if(str == null) return;
    	if(Settings.getReference().getBoolean("debug")){
    		System.out.println("DEBUG: " + getTime() + ", " +str);
    	}
    }
    */
	
	// match new Date().toString() in log files 
	public static String getDateStampToday() {
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("EEEEE");
		String day = dateFormat.format(cal.getTime()).substring(0, 3);		
		dateFormat = new SimpleDateFormat("MMMMM");
		String month = dateFormat.format(cal.getTime()).substring(0, 3);
		dateFormat = new SimpleDateFormat("dd");
		String mm = dateFormat.format(cal.getTime());
		return (day + " " + month + " " + mm).trim();
	}
    
	public static String memory() {
    	String str = "";
		str += "memory : " + ((double)Runtime.getRuntime().freeMemory()
			/ (double)Runtime.getRuntime().totalMemory())*100 + "% free<br>";
		
		str += "memory total : "+Runtime.getRuntime().totalMemory()+"<br>";    
	    str += "memory free : "+Runtime.getRuntime().freeMemory()+"<br>";
		return str;
    }
	
	public static Document loadXMLFromString(String xml) {
		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;

			builder = factory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(xml));
			return builder.parse(is);

		}
		catch (SAXParseException pe) {
			Util.log("Util.loadXMLFromString() parse error:\n"+xml, null);
		}
		catch (Exception e) {
			printError(e);
		}
		return null;
	}

	// replaces standard e.printStackTrace();
	public static String XMLtoString(Document doc) {
		String output = null;
		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			output = writer.getBuffer().toString().replaceAll("\n|\r", "");
		}
		catch (Exception e) {
			printError(e);
		}
		return output;
	}

	public static void printError(Exception e) {
		System.err.println("error "+getTime()+ ":");
		e.printStackTrace();
	}
	
	public static boolean validIP(String ip) {
	    try {
	    	
	        if (ip == null || ip.isEmpty()) return false;
	        String[] parts = ip.split( "\\." );
	        if ( parts.length != 4 ) return false;
	        for ( String s : parts ) {
	            int i = Integer.parseInt( s );
	            if ( (i < 0) || (i > 255) )
	            	return false;
	        }
	        
	        if(ip.endsWith(".")) return false;
	        
	        return true;
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	}

	public static long[] readProcStat() {
		try {

			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/stat")));
			String line = reader.readLine();
			reader.close();
			String[] values = line.split("\\s+");
			long total = Long.valueOf(values[1])+Long.valueOf(values[2])+Long.valueOf(values[3])+Long.valueOf(values[4]);
			long idle = Long.valueOf(values[4]);
			return new long[] { total, idle };

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int getCPU(){
		long[] procStat = readProcStat();
		long totproc1st = procStat[0];
		long totidle1st = procStat[1];
		Util.delay(100);
		procStat = readProcStat();
		long totproc2nd = procStat[0];
		long totidle2nd = procStat[1];
		int percent = (int) ((double) ((totproc2nd-totproc1st) - (totidle2nd - totidle1st))/ (double) (totproc2nd-totproc1st) * 100);
//		State.getReference().set(values.cpu, percent);
		return percent;
	}
	
	

	public static long[] readProcStat(String pid) {
		try {

			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/"+pid+"/stat")));
			String line = reader.readLine();
			reader.close();
			String[] values = line.split("\\s+");
			long total = Long.valueOf(values[1])+Long.valueOf(values[2])+Long.valueOf(values[3])+Long.valueOf(values[4]);
			long idle = Long.valueOf(values[4]);
			return new long[] { total, idle };

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int getCPU(String pid){
		long[] procStat = readProcStat();
		long totproc1st = procStat[0];
		long totidle1st = procStat[1];
		Util.delay(100);
		procStat = readProcStat();
		long totproc2nd = procStat[0];
		long totidle2nd = procStat[1];
		int percent = (int) ((double) ((totproc2nd-totproc1st) - (totidle2nd - totidle1st))/ (double) (totproc2nd-totproc1st) * 100);
	//	State.getReference().set(values.cpu, percent);
		return percent;
	}
	
/*
	public static String getRed5PID(){	
		
		String redPID = null;
		
		String[] cmd = { "/bin/sh", "-c", "ps -fC java" };
		
		Process proc = null;
		try { 
			proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
		} catch (Exception e) {
			Util.log("getRed5PID(): "+ e.getMessage(), null);
			return null;
		}  
		
		String line = null;
		String[] tokens = null;
		BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
		
		try {
			while ((line = procReader.readLine()) != null){
				if(line.contains("red5")) {
					tokens = line.split(" ");
					for(int i = 1 ; i < tokens.length ; i++) {
						if(tokens[i].trim().length() > 0) {
							if(redPID==null) redPID = tokens[i].trim();							
						}
					}
				}	
			}
		} catch (IOException e) {
			Util.log("getRed5PID(): ", e.getMessage());
		}

		return redPID;
	}	
	*/
	
	public static String getUbuntuVersion() {
		String version = null;
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("/etc/os-release")));
			String line;
			while ((line = reader.readLine()) != null) {
				if(line.contains("VERSION_ID")) {
					version = line.split("\"")[1];
					break;
				}
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return version;
	}
	
	static String getUptime(){
		try {
			Process proc = Runtime.getRuntime().exec(new String[]{"uptime"});
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));									
			return procReader.readLine();
		} catch (Exception e) {}
		return "error";		
	}
	
	static void getLinuxUptime(){
		new Thread(new Runnable() {
			@Override
			public void run() {	
				try {
					
					Process proc = Runtime.getRuntime().exec(new String[]{"uptime", "-s"});
					BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));									
					String line = procReader.readLine();
					Date date = new SimpleDateFormat("yyyy-MM-dd h:m:s", Locale.ENGLISH).parse(line);
			//		State.getReference().set(values.linuxboot, date.getTime());
					
				} catch (Exception e) {}										
			}
		}).start();
	}
	 
	// used if network manager down 
	public static String lookupCurrentSSID(){
		String currentSSID = null;
		try {
			
			String[] cmd = new String[]{"/bin/sh", "-c", "nmcli -t -f active,ssid dev wifi"}; // works with old/new nmcli
			Process proc = Runtime.getRuntime().exec(cmd);

			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			
			while ((line = procReader.readLine()) != null) {
				if(line.contains("yes:")) {
					currentSSID = line.substring(line.indexOf(":") + 1, line.length());
					currentSSID = currentSSID.replaceAll("^'|'$",""); // nuke surrounding quotes if any (old nmcli)
				}
			}
		} catch (Exception e) {}
		return currentSSID;	
	}
	 
	/*
	public static boolean testHTTP(){

		final String ext = State.getReference().get(values.externaladdress);
		final String http = State.getReference().get(State.values.httpport);
		final String url = "http://"+ext+":"+ http +"/oculusPrime";

		if(ext == null || http == null) return false;

		try {

			log("testPortForwarding(): "+url, "testHTTP()");
			URLConnection connection = (URLConnection) new URL(url).openConnection();
			BufferedReader procReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			log("testPortForwarding(): "+procReader.readLine(), "testHTTP()");

		} catch (Exception e) {
			 log("testPortForwarding(): failed: " + url, "testHTTP()");
			return false;
		}

		return true;
	}

	public static boolean testTelnetRouter(){
		try {

			// "127.0.0.1"; //
			final String port = Settings.getReference().readSetting(GUISettings.telnetport);
			final String ext =State.getReference().get(values.externaladdress);
			log("...telnet test: " +ext +" "+ port, null);
			Process proc = Runtime.getRuntime().exec("telnet " + ext + " " + port);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			String line = procReader.readLine();
			if(line.toLowerCase().contains("trying")){
				line = procReader.readLine();
				if(line.toLowerCase().contains("connected")){
					log("telnet test pass...", null);
					return true;
				}
			}
		} catch (Exception e) {
			log("telnet test fail..."+e.getLocalizedMessage(), null);
			return false;
		}
		log("telnet test fail...", null);
		return false;
	}


	public static boolean testRTMP(){
		try {

			final String ext = "127.0.0.1"; //State.getReference().get(values.externaladdress); //
			final String rtmp = Settings.getReference().readRed5Setting("rtmp.port");

			log("testRTMP(): http = " +ext, null);

			Process proc = Runtime.getRuntime().exec("telnet " + ext + " " + rtmp);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			String line = procReader.readLine();
			log("testRTMP(): " + line, null);
			line = procReader.readLine();
			log("testRTMP():" + line, null);
			log("testRTMP(): process exit value = " + proc.exitValue(), null);

			if(line == null) return false;
			else if(line.contains("Connected")) return true;

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public static String getJavaStatus(){

		if(redPID==null) return "jetty not running";

		String line = null;
		try {

			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/"+ redPID +"/stat")));
			line = reader.readLine();
			reader.close();
			log("getJavaStatus:" + line, null);

		} catch (Exception e) {
			printError(e);
		}

		return line;
	}

	public static String getRed5PID(){	
		
		if(redPID!=null) return redPID;
		
		String[] cmd = { "/bin/sh", "-c", "ps -fC java" };
		
		Process proc = null;
		try { 
			proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
		} catch (Exception e) {
			Util.log("getRed5PID(): "+ e.getMessage(), null);
			return null;
		}  
		
		String line = null;
		String[] tokens = null;
		BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
		
		try {
			while ((line = procReader.readLine()) != null){
				if(line.contains("red5")) {
					tokens = line.split(" ");
					for(int i = 1 ; i < tokens.length ; i++) {
						if(tokens[i].trim().length() > 0) {
							if(redPID==null) redPID = tokens[i].trim();							
						}
					}
				}	
			}
		} catch (IOException e) {
			Util.log("getRed5PID(): ", e.getMessage());
		}

		return redPID;
	}	
	
	
	public static String pingWIFI(final String addr){
		if(addr==null) return null;	
		String[] cmd = new String[]{"ping", "-c1", "-W1", addr};
		long start = System.currentTimeMillis();
		Process proc = null;
		try { 
			proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
		} catch (Exception e) {
			Util.log("pingWIFI(): "+ e.getMessage(), null);
			return null;
		}  
		
		String line = null;
		String time = null;
		BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
		
		try {
			while ((line = procReader.readLine()) != null){
				if(line.contains("time=")) {
					time = line.substring(line.indexOf("time=")+5, line.indexOf(" ms"));
					break;
				}	
			}
		} catch (IOException e) {
			Util.log("pingWIFI(): ", e.getMessage());
		}

		if(proc.exitValue() != 0 ) Util.debug("pingWIFI(): exit code: " + proc.exitValue(), null);
		if(time == null) Util.log("pingWIFI(): null result for address: " + addr, null);
		if((System.currentTimeMillis()-start) > 1100)
			Util.debug("pingWIFI(): ping timed out, took over a second: " + (System.currentTimeMillis()-start));
		
		return time;	
	}
	*/
	
	public static boolean activePID(final String pid){
		Process proc = null;
		try {

			final String[] cmd = new String[]{"/bin/sh", "-c", "ps -aux | grep " + pid};
			proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));	
			
			String line = null;
			while ((line = procReader.readLine()) != null){
				if (!line.contains("grep")){
					// log("grep: "+ line );
					final String tokens[] = line.trim().split("\\s+");
					if (tokens[1].equals(pid)){
						// log("activePID(): pid found: "+ line );
						return true;
					}
				}
			}
		} catch (Exception e) {printError(e);}
		
		// log("activePID(): pid NOT found: "+ pid);
		return false;
	}
	
	public static boolean killPID(final String pid){
		long start = System.currentTimeMillis();
		
		// ask nice first and wait for proc to close resources  
		systemCall("kill -SIGINT " + pid);
		for (int j = 0; j < 20 ; j++) {
			if (activePID(pid)) delay(10);
			else break; // dead, move on..  
		}
		
		// test if dead 
		while(activePID(pid)){
			
			// now force kill 
			log("killPID("+pid+") need to force termination.");
			systemCall("kill -9 " + pid);
			
			// don't get stuck in loop, time out.. 
			if(System.currentTimeMillis()-start > 10000){ 
				log("killPID("+pid+") timeout, pid will not die ");
				return false;
			}
			
			// don't send too many kill commands, wait again.. 
			for (int j = 0; j < 20 ; j++) {
				if (activePID(pid)) delay(10);
				else break; // dead, move on..  
			}
		}

		log("killPID("+pid+") took ms: " + (System.currentTimeMillis() - start));
		return activePID(pid);
	}
	
	public static void updateLocalIPAddress(){	
//		State state = State.getReference();
		String wdev = lookupWIFIDevice();
		
		try {			
			String[] cmd = new String[]{"/bin/sh", "-c", "ifconfig"};
			Process proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
			
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
			while ((line = procReader.readLine()) != null) {	
				if(line.contains(wdev)) {
					line = procReader.readLine();
					String addr = line.substring(line.indexOf(":")+1); 
					addr = addr.substring(0, addr.indexOf(" ")).trim();
//					if(validIP(addr)) {
//						if (!addr.equals(state.get(values.localaddress)))
//							state.set(values.localaddress, addr);
//					} else Util.debug("Util.updateLocalIPAddress(): bad address ["+ addr + "]", null);
				}
			}
		} catch (Exception e) {
			Util.log("updateLocalIPAddress(): failed to lookup wifi device: " + wdev, null);
//			state.delete(values.localaddress);
//			updateEthernetAddress();
		}
	}
	
	public static void updateEthernetAddress(){	
//		State state = State.getReference();
		try {			
			String[] cmd = new String[]{"/bin/sh", "-c", "ifconfig"};
			Process proc = Runtime.getRuntime().exec(cmd);
			proc.waitFor();
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
			while ((line = procReader.readLine()) != null) {	
				if(line.contains("eth")) {
					line = procReader.readLine();
					String addr = line.substring(line.indexOf(":")+1); 
					addr = addr.substring(0, addr.indexOf(" ")).trim();
									
//					if(validIP(addr)) State.getReference().set(values.localaddress, addr);
//					else Util.debug("Util.updateEthernetAddress(): bad address ["+ addr + "]", null);
				}
			}
		} catch (Exception e) {
//			state.set(values.localaddress, "127.0.0.1");
		}
		
//		if(!state.exists(values.localaddress)) state.set(values.localaddress, "127.0.0.1");
	}

	public static String lookupWIFIDevice(){
		String wdev = null;
		try { // this fails if no wifi is enabled 
			Process proc = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "nmcli dev"});
			String line = null;
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
			while ((line = procReader.readLine()) != null) {
				if( ! line.startsWith("DEVICE") && (line.toLowerCase().contains("wireless") || line.toLowerCase().contains("wifi"))){
					String[] list = line.split(" ");
					wdev = list[0];
				}
			}
		} catch (Exception e) {
			Util.log("lookupDevice():  no wifi is enabled  ", null);
		}
		
		return wdev;
	}
/*
	public static void updateExternalIPAddress(){
//		State state = State.getReference();
		String address = readUrlToString("http://www.xaxxon.com/xaxxon/checkhost");
//		if(validIP(address)) state.set(values.externaladdress, address);
//		else state.delete(values.externaladdress);
	}
*/
	
	public static File[] getFiles(final String match, final String folder){
		
		File dir = new File(folder);
		if( ! dir.exists()) return null; 
		if( ! dir.isDirectory()) return null;
		
		File[] names = dir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) { 
				return pathname.getName().contains(match); //.matches(match);
			}
		});
		return names;
	}
	
	static File[] getFilesDate(final String folder, final String t){
		
		long time = System.currentTimeMillis(); 
		
		try {
			time = Long.parseLong(t);
		} catch (NumberFormatException e) {
			printError(e);
		}
		
		return getFilesDate(folder, time);
	}
	
	static File[] getFilesDate(final String folder, final long time){
		
		File dir = new File(folder);
		if( ! dir.exists()) return new File[]{};
		if( ! dir.isDirectory()) return new File[]{};
	
		File[] names = dir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) { 		
				if(pathname.isFile()) {
					if(pathname.lastModified() > time) return true;
				}

				return false;
			}
		});
		
		return names;
	}
	
	public static void sortFiles(File[] files) {
		Arrays.sort(files, new Comparator<File>(){
			public int compare( File f1, File f2){
                long result = f2.lastModified() - f1.lastModified();
                if( result > 0 ){ return 1;
                } else if( result < 0 ){ return -1;
                } else return 0;
            }
        });	
	}
	
	public static void archiveLogFiles(){	
		
		String list = Settings.settingsfile + " ";
		File[] files = new File(Settings.logfolder).listFiles();
	    for(int i = 0; i < files.length; i++)
	    	if(files[i].isFile()) list += files[i].getAbsoluteFile() + " ";
	    
		final String path = "./log/archive/logs_" + System.currentTimeMillis() + ".tar ";
		final String[] cmd = new String[]{"/bin/sh", "-c", "tar -cf " + path + list};
		Util.log("archiveLogFiles(): " + path);
		new File(Settings.archivefolder).mkdir();
		new Thread(new Runnable() { public void run() {
			try { 
				Runtime.getRuntime().exec(cmd); 
			} catch (Exception e){
				printError(e);
			}
		}}).start();
	}
	
	public static String archiveImages(){
		final String path = "./log/archive/frames_" + System.currentTimeMillis() + ".tar";
		final String[] cmd = new String[]{"/bin/sh", "-c", "tar -cf " + path + " " + Settings.framefolder};
		new File(Settings.archivefolder).mkdir();
		new Thread(new Runnable() { public void run() {
			try { Runtime.getRuntime().exec(cmd); } catch (Exception e){printError(e);}
		}}).start();
		return path;
	}
	
	public static String archiveStreams(){
		final String path = "./log/archive/streams_" + System.currentTimeMillis() + ".tar";
		final String[] cmd = new String[]{"/bin/sh", "-c", "tar -cf " + path + " " + Settings.streamfolder};
		new File(Settings.archivefolder).mkdir();
		new Thread(new Runnable() { public void run() {
			try { Runtime.getRuntime().exec(cmd); } catch (Exception e){printError(e);}
		}}).start();
		return path;
	}
	
	/*// tar -czvf my_directory.tar.gz -C my_directory .
	public static String archiveTelnetScripts(){
		if( ! new File(Settings.telnetscripts).exists()) return null;
		final String path = "./log/archive/telnet_scripts_" + System.currentTimeMillis() + ".tar";
		final String[] cmd = new String[]{"/bin/sh", "-c", "tar -cf " + path + " -C " + Settings.telnetscripts + " . "};
		new Thread(new Runnable() { public void run() {
			try { Runtime.getRuntime().exec(cmd); } catch (Exception e){printError(e);}
		}}).start();
		return path;
	}
	*/
	
	/* not needed?
	public static void compressFiles(final String fname, final String[] files){
		String args = "";
		for(int i = 0 ; i < files.length ; i++) args += files[i] + " ";
		final String[] cmd = new String[]{"/bin/sh", "-c", "tar -jcf " + fname + " " + args};
		log("file created: " + fname, null);
		new Thread(new Runnable() { public void run() {
			try { Runtime.getRuntime().exec(cmd); } catch(Exception e){printError(e);}
		}}).start();
	}

	public static boolean archivePID(){ 
		Process proc = null;
		String line = null;
		try { 
			proc = Runtime.getRuntime().exec( new String[]{ "/bin/sh", "-c", "ps -a" });
			proc.waitFor();
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));	
			while ((line = procReader.readLine()) != null){
				if(line.contains("tar") || line.contains("zip")){
					Util.debug("archivePID(): found: " + line);
					return true;
				}
			}
		} catch (Exception e){return false;};
		
		return false;
	}

	public static boolean waitForArchive(){
		if(archivePID()){ 
			long start = System.currentTimeMillis();
			for(;;){
				if(archivePID()){
					delay(5000);
					debug("waitForArchive(): seconds: " + (System.currentTimeMillis() - start)/1000);
					if((System.currentTimeMillis() - start) > TEN_MINUTES){
						log("waitForArchive(): timout: " + (System.currentTimeMillis() - start)/1000, null);
						break;
					}
				} else break;
			}
			debug("waitForArchive(): exit: " + (System.currentTimeMillis() - start)/1000 + " seconds");
		}
		
		return archivePID();
	}
	*/
	
	public static Vector<File> walk(String path, Vector<File> allfiles){
        File root = new File( path );
        File[] list = root.listFiles();
        
        if(list == null) return allfiles;

        for( File f : list ) {
        	if ( !f.isHidden() && 
        		 !f.getName().startsWith("#") && 
        		 !f.getName().startsWith("$") &&
        		 !f.getName().startsWith(".") &&
        		 !f.getName().toLowerCase().contains("recycle")) {
	        	if (f.isDirectory()) walk( f.getAbsolutePath(), allfiles );
	            else allfiles.add(f);
        	}
        }   
        
        return allfiles;
	 }
	
	public static Vector<File> walkFolders(String path, Vector<File> allfolders){
        File root = new File( path );
        File[] list = root.listFiles();
        
        if(list == null) return allfolders;

        for( File f : list ) {
        	if (f.isDirectory() && !f.isHidden() &&
            	!f.getName().toLowerCase().contains("system") &&  
        		!f.getName().toLowerCase().contains("recycle") &&
        		!f.getName().toLowerCase().contains("archive") &&
        		!f.getName().startsWith("#") && 
        		!f.getName().startsWith("$") && 
        		!f.getName().contains(".") ) {
        		if ( ! allfolders.contains(f)) allfolders.addElement(f);
        		walkFolders( f.getAbsolutePath(), allfolders );
        	}
        }   
        
        return allfolders;
	 }

	public static int diskFullPercent(){
		try {			
			String line = null;
			String[] cmd = { "/bin/sh", "-c", "df" };
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
			while((line = procReader.readLine()) != null){  	
				if(line.startsWith("/")){
					line = line.substring(0, line.length()-2).trim();
					if(line.contains("%")){
						line = line.substring(line.lastIndexOf(" "), line.length()-1);
						int val = Integer.parseInt(line.trim());
						return val;
					}
				}
			}
		} catch (Exception e){}
		return Settings.ERROR;
	}
	/*
	public static String getLinuxUser(){ 
		try {			
			String line = null;
			String[] cmd = { "/bin/sh", "-c", "who" };
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
			while((line = procReader.readLine()) != null){  	
				return line.split("\\s+")[0];
			}
		} catch (Exception e){}
		return null;
	}
	
	public static Vector<String> getLinuxWho(){ 
		Vector<String> who = new Vector<String>();
		try {		
			String line = null;
			String[] cmd = { "/bin/sh", "-c", "who" };
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));					
			while((line = procReader.readLine()) != null) who.add(line);
		} catch (Exception e){}
		return who;
	}
	*/
	
	private static String getFileExtension(File file) {
	    String name = file.getName();
	    try {
	        return name.substring(name.lastIndexOf(".") + 1);
	    } catch (Exception e) {
	        return "";
	    }
	}
	
	private static String getFileText(File file) {
	    final String name = file.getName();
	   // System.out.println(name + " " + name.lastIndexOf('.'));
	    try {
	        return name.substring(0, name.lastIndexOf('.'));
	    } catch (Exception e) {
	        System.out.println(" ----- " + file.getAbsolutePath());
	        return "";
	    }
	}
	

	static String trimNumbers(String name){
		while (Character.isDigit(name.charAt(0))){	
			name = name.substring(1);
		}
		
		return name.trim().toLowerCase();
	}
	
	public static long countAllMbytes(final String path){ 
		if( ! new File(path).exists()) return 0;
		Vector<File> f = new Vector<>();
		f = walk(path, f);
		long total = 0;
		for(int i = 0 ; i < f.size() ; i++) total += f.get(i).length();
		return total / (1000*1000);
	}
	
	public static long countMbytes(final String path){ 
		Vector<File> f = new Vector<>();
		f = walk(path, f);
		long total = 0;
		for(int i = 0 ; i < f.size() ; i++) total += f.get(i).length();
		return total / (1000*1000);
	 }
	
	public static long countFilesMbytes(final String path){ 
		if( ! new File(path).isDirectory()) return 0;
		File[] f = new File(path).listFiles();
		long total = 0;
		for(int i = 0 ; i < f.length ; i++) total += f[i].length();
		return total / (1000*1000);
	 }
	
	public static long countFiles(final String path){ 
		Vector<File> f = new Vector<>();
		f = walk(path, f);
		return f.size();
	}

	static String hostName(){
		try {
			String[] cmd = new String[]{"hostname"};
			Process proc = Runtime.getRuntime().exec(cmd);
			BufferedReader procReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			return procReader.readLine();
		} catch (Exception e) {
			log("hostName(): exception: " + e.getLocalizedMessage());
		}
		return "error";
	}
	
	/*
	public static long msUntilTarget(int targetHour) {
		final int hours = getHoursUntilTarget( targetHour ) - 1;
		final int mins = Calendar.getInstance().get(Calendar.MINUTE);
		final int secs = Calendar.getInstance().get(Calendar.SECOND);	
	
		Util.log("target hour  : " + targetHour, this);
		Util.log("hours until  : " + hours, this);
		Util.log("minutes left : " + (60-mins), this);
		Util.log("seconds left : " + (60-secs), this);
		Util.log("hours   ms   : " + TimeUnit.HOURS.toMillis(hours) , this);
		Util.log("minutes ms   : " + TimeUnit.MINUTES.toMillis(60-mins), this);
	
		return TimeUnit.HOURS.toMillis(hours) + TimeUnit.MINUTES.toMillis(60-mins) - TimeUnit.SECONDS.toMillis(60-secs);
	}

	static int getHoursUntilTarget(int targetHour) {
		Calendar calendar = Calendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		return hour < targetHour ? targetHour - hour : targetHour - hour + 24;
	}
		*/
	
	private static String filterName(final File file) {
		final String ext = getFileExtension(file).toLowerCase(); 
		String text = getFileText(file).toLowerCase();
		text = text.replace(".", " ");
		
		/*
		text = text.replace(" ac3", " ");
		text = text.replace(" brrip", " ");
		text = text.replace(" xvid", " ");
		text = text.replace(" hdrip", " ");
		// text = text.replace(" dvdrip", " ");
		text = text.replace(" hdtv", " ");
		text = text.replace(" 720p", " ");
		text = text.replace(" hdtv", " ");
		text = text.replace(" web-dl", " ");
		text = text.replace(" xxx ", " ");
		text = text.replace(" x264 ", " ");
		text = text.replace(" h264 ", " ");
		text = text.replace(" ddp5 ", " ");
		text = text.replace("[torrentcouch com]", " ");
		text = text.replace("web dl esubs ", " ");*/
		//text = text.replace("5 1ch bluray reenc deejayahmed", " "); 
		
		text = text.replace(" 1080p", " ");
		text = text.replaceAll("_", " ");
		text = text.replaceAll("-", " ");
		text = text.replaceAll("  ", " ");
		text = text.replaceAll("  ", " ");
		
		if (text.startsWith("the"))		
			text = text.replace("the", "");
		
		if (text.startsWith("]"))		
			text = text.substring(1);
		
		while (text.indexOf('(') != -1) text = text.replace('(', '[');
		while (text.indexOf(')') != -1) text = text.replace(')', ']');

		String t = text.trim() + "." + ext;
		return t.trim();
	} 
	
	private static String filterFolderName(final File file) {

		String text = file.getName().toLowerCase();
		text = text.replace(".", " ");
		
		/*
		text = text.replace(" ac3", " ");
		text = text.replace(" brrip", " ");
		text = text.replace(" xvid", " ");
		text = text.replace(" hdrip", " ");
		text = text.replace(" dvdrip", " ");
		text = text.replace(" hdtv", " ");
		text = text.replace( "720p", " ");
		text = text.replace(" 1020p", " ");
		text = text.replace(" hdtv", " ");
		text = text.replace(" web-dl", " ");
		text = text.replace(" xxx ", " ");
		text = text.replace(" x264 ", " ");
		text = text.replace(" h264 ", " ");
		*/
		text = text.replace(" aac", " ");
//		text = text.replaceAll("_", " ");
//		text = text.replaceAll("-", " ");
		text = text.replaceAll("  ", " ");
		text = text.replaceAll("  ", " ");
		
		if (text.startsWith("the"))		
			text = text.replace("the", "");
		
		while (text.indexOf('(') != -1) text = text.replace('(', '[');
		while (text.indexOf(')') != -1) text = text.replace(')', ']');

		//if (getYear(text) == -1) System.out.println("year error: " + text);
		
		return text.trim();
	} 
	
	// the running man [xxxx]  
	public static int getYear(final String name){
		
		// System.out.println("==== dir name: " + name);

		int first = name.indexOf("[");
		int last = name.indexOf("]");
	
		// System.out.println(name + ": " + first + " " + last);
		if (first == -1 || last == -1){
			return -1;	
		}
		
		String numbers = name.substring(first, last);
		if (isInteger(numbers)) return Integer.parseInt(numbers);
		
		return -1;
	}
	
	public static void rename(final File file, final String newname){
		
		new Runnable() {
			@Override
			public void run() {

				final String path = file.getAbsolutePath().replace(file.getName(), "");
	 			File rename = new File(path + newname);
	 			if (rename.exists()) {
	 				
	 				c++;
	 				System.out.println(c++ + "-- delete exists: "+file.getAbsolutePath());
	 				file.delete(); // duplicate 
//					System.out.println("-- exisits -- "+file.getAbsolutePath());
	 				return;
	 				
	 			} else {
	 				
	 			    System.out.println("-- try rename ["+file.getAbsolutePath() + "]");
					System.out.println("-- try rename ["+rename.getAbsolutePath() + "]");								
	
	 				try {
	 					
	 					// java.nio.file.Files.move(
	 				//	Path to = new Paths.get(file.getAbsolutePath());
	 						//rename); //,  StandardCopyOption.REPLACE_EXISTING);
	 					 
	 				//	Path p = FileSystems.getDefault().getPath(file.getParent(), file.getName());
	 				// 	System.out.println("path: " + p);
	 				
						if (file.renameTo(rename)){
							
							System.out.println("renamed ok: " + rename.getAbsolutePath());
						
						} else {
						
							System.out.println("failed: " + rename.getAbsolutePath());
	
						}
					
	 					
					} catch (Exception e) {
						System.out.println(e.getMessage());
						e.printStackTrace();
					}	 			
	 			}	
			}
		}.run();
	}
	
	public static Vector<File> removeEmptyFolders(Vector<File> folders){
		for( int i = 0 ; i < folders.size() ; i++ ){
			if (folders.get(i).isDirectory()){
				if ( folders.get(i).listFiles().length == 0){
				
					System.out.println("\n removeEmptyFolders, empty dir: " + folders.get(i));	
					if (folders.get(i).delete()) folders.remove(i);
					else { 
						
						System.out.println("\n removeEmptyFolders, can't remove dir: " + folders.get(i).toString());
						if (folders.get(i).exists()) {
							System.out.println("\n removeEmptyFolders, still ccan't delete: " + folders.get(i).toString());
							folders.get(i).delete();
						}
					}
				}
			}
		}
		
		return folders;
	}
	
	public static boolean isMediaFile(final File file) {
		
		if (file.isDirectory()) return false;
		
		Vector<String> filter = new Vector<String>();
		filter.add("mp3");
		filter.add("m4a");
		filter.add("mkv");
		filter.add("flac");
		filter.add("zip");
		filter.add("mov");
		filter.add("gif");
		filter.add("mp4");
		filter.add("wmv");
		filter.add("rar");
		filter.add("m4a");
		filter.add("mkv");
		filter.add("flac");
		filter.add("zip");
		filter.add("mov");
		filter.add("png");
		filter.add("jpg");
		filter.add("jpeg");
		filter.add("avi");
		filter.add("zip");
		filter.add("mov");
		filter.add("png");
		filter.add("jpg");
		filter.add("jpeg");

		final String ext = getFileExtension(file).toLowerCase().trim();
		if (ext != null) return filter.contains(ext);
		
		return false;
	}
	

	public static Vector<File> purgeMediaOnly(Vector<File> files){
		for( int i = 0 ; i < files.size() ; i++ ){
			
			if (!isMediaFile(files.get(i))){
				
	//			if ( files.get(i).delete()){
					
					System.out.println("not media.. deleted ok: " + files.get(i).getAbsoluteFile());
					
	//			}
			}
		}
		
		return files;
	}
	

	public static void purgeMeta(Vector<File> files){
		for( int i = 0 ; i < files.size() ; i++ ){
			if (files.get(i).getName().toLowerCase().contains(".ds_") || files.get(i).getName().contains("thumbs")) {	
	//			if ( files.get(i).delete()){
					
					System.out.println("not media.. deleted ok: " + files.get(i).getAbsoluteFile());
					
	//			}
			}
		}
	}
	
	
	public static Vector<File> filterMediaOnly(final Vector<File> files){
		Vector<File> filtered = new Vector<File>();
		for( int i = 0 ; i < files.size() ; i++ )
			if (isMediaFile(files.get(i))) 
				filtered.add(files.get(i));
		return filtered;
	}
	
	public static void doRename(final String path){
		
		if ( ! new File(path).exists()) {
			System.out.println("path not found: " + path);
			System.exit(-1);
		}
	
		Vector<File> f = walk(path, new Vector<File>());

	//	Vector<File> remove = filterMediaOnly(files);
	//	for( int i = 0 ; i < remove.size() ; i++ ) System.out.println("--- "+remove.get(i).getName()); 
	
		System.out.println("----   files found: " + f.size());
		Vector<File> files = filterMediaOnly(f);
		System.out.println("----   files purge: " +  files.size());
		
		for( int i = 0 ; i < files.size() ; i++ ){
		
			System.out.println("file: " + files.get(i).getAbsolutePath());
			final String filter = filterName(files.get(i));	
			if (filter != null) {
				if ( !files.get(i).getName().equals(filter)){
						
		//				System.out.println(" --- file:   " + files.get(i).getAbsolutePath());
		//				System.out.println(" --- filter: " + filter);
						rename(files.get(i), filter);
			
			
				}	
			}
		}

		delay(999);
		
		Vector<File> folders = walkFolders(path, new Vector<File>());
	
		System.out.println("-- startup folders found: " + folders.size());	
//		folders = removeEmptyFolders(folders);
//		System.out.println("-- startup purge empty found: " + folders.size());	
		
		
		for( int i = 0 ; i < folders.size() ; i++ ){
	
			System.out.println("dir: " + folders.get(i).getAbsolutePath());
	
			final String rename = filterFolderName(folders.get(i)); 
			final String newpath = folders.get(i).getAbsolutePath().replace(folders.get(i).getName(), rename);
		
			if ( ! folders.get(i).getAbsolutePath().equals(newpath)){
				
				
				System.out.println("_from: " + folders.get(i).getAbsolutePath());
				System.out.println("_to:   " + newpath);
				
				try {
				
					if (folders.get(i).renameTo(new File(newpath))){
						
						System.out.println("folders yes renamed");
						
					} else {
						
						System.out.println("folders not renamed");
						
					}
			
				} catch (Exception e) {
				
					System.out.println("error dir: " + newpath);
					System.out.println(e.getMessage());
					e.printStackTrace();
					System.exit(-1);
					
				}	
			}
		
			
			 removeEmptyFolders(folders);
			 removeEmptyFolders(folders);
		}
		
	}	
    
	public static void main(String[] args) throws Exception {
    	final long start = System.currentTimeMillis();
		System.out.println("---- START ----- ");

    	doRename("Y:\\");
		
    	// Vector<File> files = new Vector<File>();
		// Vector<File> files = walk("Z:\\", new Vector<File>());
		// purgeMeta(files);
		// System.out.println("----   files found: " + files.size());
		
		System.out.println("---- DONE: " + (System.currentTimeMillis()-start)/1000 + " seconds ----- ");

    }
	
}

