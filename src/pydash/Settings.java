package pydash;
import java.io.*;
import java.util.HashMap;
import java.util.UUID;

public class Settings {
	
	public final static String sep = "/";
	public final static String redhome        = System.getenv("RED5_HOME");
	public final static String appsubdir      = "webapps/oculusPrime";

	public final static String streamfolder   = redhome + "/webapps/oculusPrime/streams/";
	public final static String framefolder    = redhome + "/" + appsubdir+"/framegrabs";
	public final static String streamsfolder  = redhome + "/" + appsubdir+"/streams";
	public final static String settingsfile   = redhome + "/conf/oculus_settings.txt";
	public final static String stdout         = redhome + "/log/jvm.stdout";
	public final static String telnetscripts  = redhome + "/telnet_scripts"; 
	public final static String archivefolder  = redhome + "/log/archive";
	public final static String logfolder      = redhome +"/log";
	
	public final static String DISABLED = "disabled";
	public final static String ENABLED  = "enabled";
	public static final String FALSE    = "false"; // should be boolean.false.toString? 
	public static final String TRUE     = "true";	
	public static final int ERROR       = -1;

	private static Settings singleton = new Settings();
	public static Settings getReference() { return singleton; }
	private HashMap<String, String> settings = new HashMap<String, String>(); 
	
	private Settings(){
		
		// be sure of basic configuration 
		// if(! new File(settingsfile).exists()) createFile(settingsfile);
		
		// importFile();
	}
	
	private void importFile(){
		try {
			String line;
			FileInputStream filein = new FileInputStream("settingsfile");
			BufferedReader reader = new BufferedReader(new InputStreamReader(filein));
			while ((line = reader.readLine()) != null) {
				String items[] = line.split(" ");
				settings.put(items[0], items[1]);
			}
			reader.close();
			filein.close();
		} catch (Exception e) {
			Util.log("importFile: " + e.getMessage(), this);
		}
	}
		
	public boolean getBoolean(String key) {
		if (key == null) return false;
		String str = readSetting(key);
		if (str == null) return false;
		if (str.toUpperCase().equals("YES")) return true;
		else if (str.equalsIgnoreCase(TRUE)) return true;
		return false;
	}

	public int getInteger(String key) {

		String ans = null;
		int value = ERROR;

		try {
			ans = readSetting(key);
			value = Integer.parseInt(ans);
		} catch (Exception e) {
			return ERROR;
		}

		return value;
	}

	public double getDouble(String key) {

		String ans = null;
		double value = ERROR;

		try {
			ans = readSetting(key);
			value = Double.parseDouble(ans);
		} catch (Exception e) {
			return ERROR;
		}

		return value;
	}

	public String readSetting(String str) {
		
		if(settings.containsKey(str)) return settings.get(str);
	
		// reads++;
		FileInputStream filein;
		String result = null;
		try {

			filein = new FileInputStream(settingsfile);
			BufferedReader reader = new BufferedReader(new InputStreamReader(filein));
			String line = "";
			while ((line = reader.readLine()) != null) {
				String items[] = line.split(" ");
				if(items.length>=2){
					if ((items[0].toUpperCase()).equals(str.toUpperCase())) {
						result = items[1];
					}
				} 
			}
			reader.close();
			filein.close();
		} catch (Exception e) {
			Util.log("readSetting: " + e.getMessage(), this);
			return null; 
		}
		
		// don't let string "null" be confused for actually a null, error state  
		if(result!=null) if(result.equalsIgnoreCase("null")) result = null;
		
		return result;
	}
/*
	@Override
	public String toString(){
		
		String result = new String();
		for (GUISettings factory : GUISettings.values()) {
			String val = readSetting(factory.toString());
			if (val != null) { // never send out plain text passwords 
				if( ! factory.equals(GUISettings.email_password))
					result += factory.toString() + " " + val + "<br>"; 
			}
		}
	
		for (ManualSettings ops : ManualSettings.values()) {
			String val = readSetting(ops.toString());
			if (val != null) result += ops.toString() + " " + val + "<br>"; 
		}
		
		return result;
	}
*/
	
	public synchronized void createFile(String path) {
		try {
			
			Util.log("creating "+path.toString(), this);
			
			FileWriter fw = new FileWriter(new File(path));
	
			/*
			fw.append("# GUI settings \r\n");
			for (GUISettings factory : GUISettings.values()) {
				fw.append(factory.toString() + " " + GUISettings.getDefault(factory) + "\r\n");
			}
			
			fw.append("# manual settings \r\n");
			for (ManualSettings ops : ManualSettings.values()) {
				fw.append(ops.toString() + " " + ManualSettings.getDefault(ops) + "\r\n");
			}
			*/
			
			fw.append("# user list \r\n");
			fw.append("salt "+UUID.randomUUID().toString() + "\r\n");
			fw.close();

		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	/** Organize the settings file into 3 sections. Use Enums's to order the file */
	public synchronized void writeFile(){
		try {
			
			final String temp = redhome +"/conf/oculus_created.txt";
			FileWriter fw = new FileWriter(new File(temp));
			
			fw.append("# gui settings \r\n");
			/*
			for (GUISettings factory : GUISettings.values()) {
				String val = readSetting(factory.toString());
				if (val != null) {
					fw.append(factory.toString() + " " + val + "\r\n");
				} 
				else {
					fw.append(factory.toString() + " " + GUISettings.getDefault(factory) + "\r\n");
				}
			}
			
			fw.append("# manual settings \r\n");
			for (ManualSettings ops : ManualSettings.values()) {
				String val = readSetting(ops.toString());
				if (val != null){
					fw.append(ops.toString() + " " + val + "\r\n");
				} 
				else {
					fw.append(ops.toString() + " " + ManualSettings.getDefault(ops) + "\r\n");
				}
			}
			
			fw.append("# user list \r\n");
			if(readSetting("salt") != null) { fw.append("salt " + readSetting("salt") + "\r\n"); }
			else fw.append("salt "+UUID.randomUUID().toString() + "\r\n");

			if(readSetting("user0")!=null){
				String[][] users = getUsers();
				for (int j = 0; j < users.length; j++) {
					fw.append("user" + j + " " + users[j][0] + "\r\n");
					fw.append("pass" + j + " " + users[j][1] + "\r\n");
				}
			}
			
			*/
			fw.close();
			
			// now swap temp for real file
			new File(settingsfile).delete();
			new File(temp).renameTo(new File(settingsfile));
			new File(temp).delete();

			importFile();
			
		} catch (Exception e) {
			Util.log("Settings.writeFile(): " + e.getMessage(), this);
		}
	}
 
	public void writeSettings(String setting, int value) {
		String str = null;
		try {
			str = Integer.toString(value);
		} catch (Exception e) {
			return;
		}

		if (str != null) writeSettings(setting, str);
	}
	
	/**
	 * Modify value of existing setting. read whole file, replace line while
	 * you're at it, write whole file
	 */
	public synchronized void writeSettings(String setting, String value) {
		
		if(setting == null) return;
		if(value == null) return;
		if(value.equalsIgnoreCase("null")) return;
				
		setting = setting.trim();
		value = value.trim();

		if(settings.get(setting).equals(value)) {
			// Util.debug("setting rejected, "+setting+" already set to: " + value, this);
			return;
		}
		
		settings.put(setting, value);
		FileInputStream filein;
		String[] lines = new String[999];
		try {

			filein = new FileInputStream(settingsfile);
			BufferedReader reader = new BufferedReader(new InputStreamReader(filein));
			int i = 0;
			while ((lines[i] = reader.readLine()) != null) {
				String items[] = lines[i].split(" ");
				if(items.length==2){ //TODO: SHOULD SETTINGS BE CASE SENSITIVE? 
					if ((items[0].toUpperCase()).equals(setting.toUpperCase())) {
						lines[i] = setting + " " + value;
					}
				}
				i++;
			}
			filein.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		FileOutputStream fileout;
		try {
			fileout = new FileOutputStream(settingsfile);
			for (int n = 0; n < lines.length; n++) {
				if (lines[n] != null) {
					new PrintStream(fileout).println(lines[n]);
				}
			}
			fileout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void newSetting(String setting, String value) {
		setting = setting.trim();
		value = value.trim();

		FileInputStream filein;
		String[] lines = new String[999];
		try {
			filein = new FileInputStream(settingsfile);
			BufferedReader reader = new BufferedReader(new InputStreamReader(filein));
			int i = 0;
			while ((lines[i] = reader.readLine()) != null) {
				lines[i] = lines[i].replaceAll("\\s+$", ""); 
				if (!lines[i].equals("")) {
					i++;
				}
			}
			filein.close();
			settings.put(setting, value);
		} catch (Exception e) {
			e.printStackTrace();
		}

		FileOutputStream fileout;
		try {
			fileout = new FileOutputStream(settingsfile);
			for (int n = 0; n < lines.length; n++) {
				if (lines[n] != null) {
					new PrintStream(fileout).println(lines[n]);
				}
			}
			new PrintStream(fileout).println(setting + " " + value);
			fileout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteSetting(String setting) {
		// read whole file, remove offending line, write whole file
		setting = setting.replaceAll("\\s+$", ""); 
		FileInputStream filein;
		String[] lines = new String[999];
		try {
			filein = new FileInputStream(settingsfile);
			BufferedReader reader = new BufferedReader(new InputStreamReader(filein));
			int i = 0;
			while ((lines[i] = reader.readLine()) != null) {
				String items[] = lines[i].split(" ");
				if ((items[0].toUpperCase()).equals(setting.toUpperCase())) {
					lines[i] = null;
				}
				i++;
			}
			filein.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		FileOutputStream fileout;
		try {
			fileout = new FileOutputStream(settingsfile);
			for (int n = 0; n < lines.length; n++) {
				if (lines[n] != null) {
					new PrintStream(fileout).println(lines[n]);
				}
			}
			fileout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readRed5Setting(String str) {
		String filenm = System.getenv("RED5_HOME")+"/conf/red5.properties";
		FileInputStream filein;
		String result = null;
		try {
			filein = new FileInputStream(filenm);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					filein));
			String line = "";
			while ((line = reader.readLine()) != null) {
				String s[] = line.split("=");
				if (s[0].equals(str)) {
					result = s[1];
				}
			}
			filein.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public void writeRed5Setting(String setting, String value) { 
		String filenm = System.getenv("RED5_HOME")+"/conf/red5.properties";
		value = value.replaceAll("\\s+$", ""); 
		FileInputStream filein;
		String[] lines = new String[999];
		try {
			filein = new FileInputStream(filenm);
			BufferedReader reader = new BufferedReader(new InputStreamReader(filein));
			int i = 0;
			while ((lines[i] = reader.readLine()) != null) {
				String items[] = lines[i].split("=");
				if ((items[0].toUpperCase()).equals(setting.toUpperCase())) {
					lines[i] = setting + "=" + value;
				}
				i++;
			}
			filein.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		FileOutputStream fileout;
		try {
			fileout = new FileOutputStream(filenm);
			for (int n = 0; n < lines.length; n++) {
				if (lines[n] != null) {
					new PrintStream(fileout).println(lines[n]);
				}
			}
			fileout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}