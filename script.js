 var GETINFOINTERVAL = 2000;

 function loaded() {
     getInfo();
 }

 function openxmlhttp(theurl, functionname) {
     if (window.XMLHttpRequest) {
         xmlhttp = new XMLHttpRequest();
     } else if (window.ActiveXObject) {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
         theurl += "?" + new Date().getTime();
     }
     if (xmlhttp != null) {
         xmlhttp.onreadystatechange = functionname; // event handler function call;
         xmlhttp.open("GET", theurl, true);
         xmlhttp.send(null);
     } else {
         alert("Your browser does not support XMLHTTP.");
     }
 }

 function getInfo() {
     openxmlhttp("?action=getinfo", infoReceived);
     setTimeout("getInfo();", GETINFOINTERVAL);
 }
 
 
 function infoReceived() {
     if (xmlhttp.readyState == 4) {
         if (xmlhttp.status == 200) {
             eval(xmlhttp.responseText);
         }
     }
 }

 function cmdkill(pid) {
	// alert("send kill: " + pid);
	openxmlhttp("?action=kill&pid="+pid, null);
 }
 
 function cmdsend(cmd) {
	//alert("send command: " + cmd);
	openxmlhttp("?action="+cmd, null);
 }
 
 function inrlink(id, str) {
	if (document.getElementById(id) != null) 
		document.getElementById(id).innerHTML = "<button type=\"button\" onclick=\"cmdkill("+ str +")\"> kill "+ str +"</button> "; 
	
 }

 function inrhtml(id, str) {
	if (document.getElementById(id) != null) 
		if (document.getElementById(id) != str)
			document.getElementById(id).innerHTML = str;
 }
 
